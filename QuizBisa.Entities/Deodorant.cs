﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace QuizBisa.Entities
{
    public class Deodorant
    {
        //<summary>
        //ini atribut DeodorantId sebagai primary key dan auto increment
        //</summary>
        [Key]
        public int DeodorantId { get; set; }

        //<summary>
        //ini atribut DeodorantName wajib diisi
        //</summary>
        [Required]
        public string DeodorantName { get; set; }

        //<summary>
        //ini atribut DeodorantCount wajib diisi
        //</summary>
        [Required]
        public int DeodorantCount { get; set; }
    }
}
