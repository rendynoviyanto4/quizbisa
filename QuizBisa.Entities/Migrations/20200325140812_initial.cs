﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QuizBisa.Entities.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "deodorant",
                columns: table => new
                {
                    DeodorantId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DeodorantName = table.Column<string>(nullable: false),
                    DeodorantCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_deodorant", x => x.DeodorantId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "deodorant");
        }
    }
}
