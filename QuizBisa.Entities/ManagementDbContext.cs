﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuizBisa.Entities
{
    public class ManagementDbContext : DbContext
    {
        public ManagementDbContext(DbContextOptions<ManagementDbContext> options):base(options)
        {

        }

        //<summary>
        //ini untuk menampung database dalam Deodorant
        //</summary>
        public DbSet<Deodorant> Deodorants { set; get; }


        //<summary>
        //method untuk bikin tabel deodorant
        //</summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Deodorant>().ToTable("deodorant");
        }
    }
}
